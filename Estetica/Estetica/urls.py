"""Estetica URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""


from django.contrib import admin
from django.contrib.auth.views import login, logout_then_login
from django.conf.urls import url
from Peluditos import views
from Peluditos.views import Inicio, ListaClientes, ListaServicios, ListaMascota, ListaCitas, AgregarCliente, AgregarCita, AgregarMascota, AgregarServicio, EditarCliente, EditarMascota,EditarServicio,EditarCita, EliminarCita, EliminarCliente, EliminarMascota,EliminarServicio, ServicioDetail

urlpatterns = [

    url(r'^admin/', admin.site.urls),
    url(r'^$', Inicio.as_view(), name= 'Inicio'),
    url(r'^accounts/login/$', login,{'template_name': 'Peluditos/Login.html'}, name= 'Login'),
    url(r'^Logout/$', logout_then_login, name= 'Logout'),
    #CRUD: CREATEVIEW
    url(r'^AgregarCliente/$', AgregarCliente.as_view(), name= 'AgregarCliente'),
    url(r'^AgregarCita/$', AgregarCita.as_view(), name= 'AgregarCita'),
    url(r'^AgregarMascota/$', AgregarMascota.as_view(), name= 'AgregarMascota'),
    url(r'^AgregarServicio/$',AgregarServicio.as_view(), name= 'AgregarServicio'),
    #CRUD: RETRIEVEVIEW
    url(r'^Clientes/$', ListaClientes.as_view(), name= 'Clientes'),
    url(r'^Servicios/$',ListaServicios.as_view(),name= 'Servicios'),
    url(r'^Mascotas/$', ListaMascota.as_view(), name= 'Mascotas'),
    url(r'^Citas/$',  ListaCitas.as_view(),    name= 'Citas'),
    #CRUD: DETAILEVIEW
    url(r'^DetallesdeServicio/(?P<pk>\d+)/$', ServicioDetail.as_view(), name= 'DetallesdeServicio'),
    #CRUD:UPDATEVIEW
    url(r'^EditarCliente/(?P<pk>\d+)/$', EditarCliente.as_view(), name= 'EditarCliente'),
    url(r'^EditarMascota/(?P<pk>\d+)/$', EditarMascota.as_view(), name= 'EditarMascota'),
    url(r'^EditarCita/(?P<pk>\d+)/$', EditarCita.as_view(), name= 'EditarCita'),
    url(r'^EditarServicio/(?P<pk>\d+)/$', EditarServicio.as_view(), name= 'EditarServicio'),
    #CRUD: DeleteView
    url(r'^EliminarCliente/(?P<pk>\d+)/$', EliminarCliente.as_view(), name= 'EliminarCliente'),
    url(r'^EliminarMascota/(?P<pk>\d+)/$', EliminarMascota.as_view(), name= 'EliminarMascota'),
    url(r'^EliminarServicio/(?P<pk>\d+)/$', EliminarServicio.as_view(), name= 'EliminarServicio'),
    url(r'^EliminarCita/(?P<pk>\d+)/$', EliminarCita.as_view(), name= 'EliminarCita')

]
