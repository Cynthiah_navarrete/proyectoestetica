# -*- coding: utf-8 -*-
# Generated by Django 1.11.29 on 2020-06-16 22:59
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Peluditos', '0004_cita'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='cita',
            name='Propietario',
        ),
        migrations.RemoveField(
            model_name='cita',
            name='Servicio',
        ),
        migrations.AddField(
            model_name='cita',
            name='Servicio',
            field=models.ManyToManyField(to='Peluditos.Servicio'),
        ),
    ]
