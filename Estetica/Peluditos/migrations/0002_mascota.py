# -*- coding: utf-8 -*-
# Generated by Django 1.11.29 on 2020-06-16 02:25
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Peluditos', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Mascota',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('Nombre', models.CharField(max_length=100)),
                ('Raza', models.CharField(max_length=100)),
                ('Color', models.CharField(max_length=100)),
                ('Sexo', models.CharField(choices=[('F', 'Femenino'), ('M', 'Masculino')], default='M', max_length=1)),
                ('Fecha_Nacimiento', models.DateField()),
                ('Peso_Kg', models.DecimalField(decimal_places=2, max_digits=5)),
                ('Propietario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Peluditos.Propietario')),
            ],
        ),
    ]
