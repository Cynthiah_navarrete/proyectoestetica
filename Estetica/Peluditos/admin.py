# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin

from .models import Propietario, Mascota, Servicio, Cita

# Register your models here.
admin.site.register(Propietario)
admin.site.register(Mascota)
admin.site.register(Servicio)
admin.site.register(Cita)
