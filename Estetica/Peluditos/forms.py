from django import forms
from .models import Propietario, Mascota, Servicio, Cita


# Cliente
class ClienteForm(forms.ModelForm):
    class Meta:
        model = Propietario
        fields = [
            'Nombre',
            'Apellido_Paterno',
            'Apellido_Materno',
            'Sexo',
            'Fecha_Nacimiento',
            'Telefono',
            'Email',
        ]
        labels = {
            'Nombre':'Nombre',
            'Apellido_Paterno':'Apellido Paterno',
            'Apellido_Materno':'Apellido Materno',
            'Sexo':'Sexo',
            'Fecha_Nacimiento':'Fecha de Nacimiento',
            'Telefono': 'Telefono',
            'Email' : 'Email',
        }

# Mascota
class MascotaForm(forms.ModelForm):
    class Meta:
        model = Mascota
        fields = [
            'Nombre',
            'Raza',
            'Color',
            'Sexo',
            'Fecha_Nacimiento',
            'Peso_Kg',
            'Propietario',
        ]
        labels = {
            'Nombre':'Nombre',
            'Raza':'Raza',
            'Color':'Color',
            'Sexo':'Sexo',
            'Fecha_Nacimiento':'Fecha de Nacimiento',
            'Peso_Kg': 'Peso en Kilogramos',
            'Propietario' : 'Propietario',
        }

# Servicio
class ServicioForm(forms.ModelForm):
    class Meta:
        model = Servicio
        fields = [
            'Tipo_de_Servicio',
            'Costo',
            'Duracion',
        ]
        labels = {
            'Tipo_de_Servicio':'Tipo de Servicio',
            'Costo':'Costo',
            'Duracion':'Tiempo de Duracion (Hr)',
        }

#Cita
class CitaForm(forms.ModelForm):
    class Meta:
        model = Cita
        fields = [
            'Fecha',
            'Servicio',
            'Mascota',
            'Estado'
        ]
        labels = {
            'Fecha':'Fecha',
            'Servicio': 'Servicio requerido',
            'Mascota' : 'Mascota',
            'Estado' : 'Estados'
        }
