# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Propietario(models.Model):
    id = models.AutoField(primary_key = True)
    Nombre = models.CharField(max_length = 100)
    Apellido_Paterno = models.CharField(max_length = 100)
    Apellido_Materno = models.CharField(max_length = 100)
    SEXOS = (("F", "Femenino"),("M", "Masculino"))
    Sexo = models.CharField(max_length = 1, choices = SEXOS, default = "M")
    Fecha_Nacimiento = models.DateField()
    Telefono = models.CharField(max_length = 10)
    Email = models.EmailField(max_length = 250, blank = True, unique = True)

    def __str__(self):
        return "{}".format(self.Nombre)

    class Meta:
        verbose_name_plural = "Propietarios"

class Mascota(models.Model):
    id = models.AutoField(primary_key = True)
    Nombre = models.CharField(max_length = 100)
    Raza = models.CharField(max_length = 100)
    Color = models.CharField(max_length = 100)
    SEXOS = (("F", "Femenino"),("M", "Masculino"))
    Sexo = models.CharField(max_length = 1, choices = SEXOS, default = "M")
    Fecha_Nacimiento = models.DateField()
    Peso_Kg = models.DecimalField(decimal_places = 2, max_digits = 5)
    Propietario = models.ForeignKey(Propietario, on_delete = models.CASCADE)

    def __str__(self):
        return "{}".format(self.Nombre)

    class Meta:
        verbose_name_plural = "Mascotas"


class Servicio(models.Model):
    id = models.AutoField(primary_key = True)
    Tipo_de_Servicio = models.CharField(max_length = 100)
    Costo = models.DecimalField(decimal_places = 2, max_digits = 65)
    Duracion = models.DecimalField(decimal_places = 2, max_digits = 5)

    def __str__(self):
        return "{}".format(self.Tipo_de_Servicio)

    class Meta:
        verbose_name_plural = "Servicios"

class Cita(models.Model):
    id = models.AutoField(primary_key = True)
    Fecha = models.DateTimeField()
    Servicio = models.ManyToManyField(Servicio, related_name='servicios')
    Mascota = models.ForeignKey(Mascota, on_delete = models.CASCADE)
    ESTADO = (("R", "Realizada"),("P", "Pendiente"))
    Estado = models.CharField(max_length = 1, choices = ESTADO, default = "P")
