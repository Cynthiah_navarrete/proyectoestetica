# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.http import HttpResponse
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView,CreateView, ListView, UpdateView, DeleteView, DetailView
from .models import Propietario, Servicio, Mascota,Cita
from .forms import ClienteForm, ServicioForm, MascotaForm, CitaForm, ClienteForm


#PAGINA PRINCIPAL
class Inicio(TemplateView):
    template_name = 'Peluditos/Inicio.html'


################ CRUDE: CLIENTE ################

#CREATEVIEW
class AgregarCliente(LoginRequiredMixin, CreateView):
    model = Propietario
    form_class = ClienteForm
    template_name = 'Peluditos/AgregarCliente.html'
    success_url = reverse_lazy('Clientes')

#RETRIEVEVIEW
class ListaClientes(LoginRequiredMixin, ListView):
    model = Propietario
    template_name = 'Peluditos/Clientes.html'
    context_object_name = 'propietarios'
    queryset = Propietario.objects.all()

#UPDATEVIEW
class EditarCliente(LoginRequiredMixin, UpdateView):
    model = Propietario
    form_class = ClienteForm
    template_name = 'Peluditos/EditarCliente.html'
    context_object_name = 'propietarios'
    success_url = reverse_lazy('Clientes')

#DELETEVIEW
class EliminarCliente(LoginRequiredMixin, DeleteView):
    model = Propietario
    template_name = "Peluditos/EliminarCliente.html"
    context_object_name = "propietarios"
    success_url = reverse_lazy('Clientes')


############### CRUDE: MASCOTA ################

#CREATEVIEW
class AgregarMascota(LoginRequiredMixin, CreateView):
    model = Mascota
    form_class = MascotaForm
    template_name = 'Peluditos/AgregarMascota.html'
    success_url = reverse_lazy('Mascotas')

#RETRIEVEVIEW
class ListaMascota(LoginRequiredMixin, ListView):
    model = Mascota
    template_name = 'Peluditos/Mascota.html'
    context_object_name = 'mascotas'
    queryset = Mascota.objects.all()

#UPDATEVIEW
class EditarMascota(LoginRequiredMixin, UpdateView):
    model = Mascota
    form_class = MascotaForm
    template_name = 'Peluditos/EditarMascota.html'
    context_object_name = 'mascotas'
    success_url = reverse_lazy('Mascotas')

#DELETEVIEW
class EliminarMascota(LoginRequiredMixin, DeleteView):
    model = Mascota
    template_name = "Peluditos/EliminarMascota.html"
    context_object_name = "mascotas"
    success_url = reverse_lazy('Mascotas')


############### CRUDE: SERVICIO ################

#CREATEVIEW
class AgregarServicio(LoginRequiredMixin, CreateView):
    model = Servicio
    form_class = ServicioForm
    template_name = 'Peluditos/AgregarServicio.html'
    success_url = reverse_lazy('Servicios')

#RETRIEVEVIEW
class ListaServicios(LoginRequiredMixin, ListView):
    model = Servicio
    template_name = 'Peluditos/Servicios.html'
    context_object_name = 'servicios'
    queryset = Servicio.objects.all()


#DETAILEVIEW
class ServicioDetail(LoginRequiredMixin, DetailView):
    model = Servicio
    template_name = 'Peluditos/Detail.html'
    context_object_name = 'servicio'


#UPDATEVIEW
class EditarServicio(LoginRequiredMixin, UpdateView):
    model = Servicio
    form_class = ServicioForm
    template_name = 'Peluditos/EditarServicio.html'
    context_object_name = 'servicios'
    success_url = reverse_lazy('Servicios')

#DELETEVIEW
class EliminarServicio(LoginRequiredMixin, DeleteView):
    model = Servicio
    template_name = "Peluditos/EliminarServicio.html"
    context_object_name = "servicios"
    success_url = reverse_lazy('Servicios')


############### CRUDE: CITAS ################

#CREATEVIEW
class AgregarCita(LoginRequiredMixin, CreateView):
    model = Cita
    form_class = CitaForm
    template_name = 'Peluditos/AgregarCita.html'
    success_url = reverse_lazy('Citas')

#RETRIEVEVIEW
class ListaCitas(LoginRequiredMixin, ListView):
    model = Cita
    template_name = 'Peluditos/Citas.html'
    context_object_name = 'citas'
    queryset = Cita.objects.all()

#UPDATEVIEW
class EditarCita(LoginRequiredMixin, UpdateView):
    model = Cita
    form_class = CitaForm
    template_name = 'Peluditos/EditarCita.html'
    context_object_name = 'citas'
    success_url = reverse_lazy('Citas')

#DELETEVIEW
class EliminarCita(LoginRequiredMixin, DeleteView):
    model = Cita
    template_name = "Peluditos/EliminarCita.html"
    context_object_name = "citas"
    success_url = reverse_lazy('Citas')
